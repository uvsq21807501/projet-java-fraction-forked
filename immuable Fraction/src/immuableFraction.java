
public final class immuableFraction {
	 //attributs :
     private double numerateur ;
	 private double denominateur;
	 
	 //constructeurs :
	 public immuableFraction(double a , double b) {
		 numerateur = a;
		 denominateur = b;
      }
	 public immuableFraction(double m_numerateur) {
		 numerateur = m_numerateur;
		 this.denominateur = 1;
	 }
	 public immuableFraction() {
		 this.numerateur=0;
		 this.denominateur=1;
	 }
	 //constatntes :
	 public static final immuableFraction ZERO = new immuableFraction(0, 1);
	 public static final immuableFraction UN = new immuableFraction(1, 1);
	 
	 //getters :
	 public double getNumerateur() {
		 return numerateur;
	 }

	 public double getDenominateur() {
		 return denominateur;
	 }

	 public String toString() {
		 return getNumerateur() + "/" + getDenominateur();	 
	 }
	
     public double afficher () {
    	 double res= getNumerateur()/getDenominateur();
    	 return res;
     }
}
